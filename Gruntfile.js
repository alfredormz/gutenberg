'use strict';

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ["data"],
    curl: {
      'data/rdf-files.tar.bz2': 'http://www.gutenberg.org/cache/epub/feeds/rdf-files.tar.bz2'
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-curl');

  grunt.registerTask('mkdir','Creates the data folder', function() {
    grunt.file.mkdir("data");
  });

  grunt.registerTask('download', ['clean', 'mkdir', 'curl']);

  grunt.registerTask('extract', function(){
    var execSync = require("exec-sync"),
        cmd = 'bunzip2 -c data/rdf-files.tar.bz2 | (cd data; tar -xf -)';

    grunt.log.write("Extracting...");

    try {
      execSync(cmd);
      grunt.log.ok();
    } catch(e) {
      grunt.log.error(e);
      grunt.fail.warn("Something went wrong.");
    }
  });

  grunt.registerTask('db_create', function(){
    //creates the db
    var config = require('config').database,
        r = require('rethinkdb'),
        done = this.async();

    r.connect({host: config.host, port:config.port}, function(err, conn){
      if(err) {
        grunt.log.error(err);
      } else {
        r.dbCreate(config.db).run(conn, function(err, result){
          if(err){
            grunt.log.writeln("Database",config.db,"already exists");
          }else{
            grunt.log.write("Database",config.db,"created successfully").ok();
          }
        });

        r.db(config.db).tableCreate("books").run(conn, function(err, result){
          if(err){
            grunt.log.writeln("Table books already exists");
          }else{
            grunt.log.write("Table books created successfully").ok();
          }
          done();
        });

      }
    });

    setTimeout(function() {
      done();
    }, 5000);
  });

  grunt.registerTask('import', function(){
    var glob = require("glob"),
        async = require("async"),
        rdfParser = require("./lib/rdf-parser"),
        gutenberg = require("./app/gutenberg");

    var finish = this.async();

    var q = async.queue(function(fileName, done){
        rdfParser(fileName, function(error, bookData){
          if(error){
            grunt.log.error(error);
          }else {
            gutenberg.addBook(bookData, function(error, result){
              if(!error){
                grunt.log.write("Saving", result.title, "...").ok();
              }else{
                grunt.log.error(error.message);
              }
              done();
            });
          }
        });

    }, gutenberg.poolSize);

    grunt.log.writeln("Importing... This is going to take a while.");

    glob("data/**/*.rdf", function(err, files){
      async.each(files, function(item){
        q.push(item);
      }, finish);
    });


  });

  grunt.registerTask('default', ["download", "extract", "db_create", "import"]);
};
