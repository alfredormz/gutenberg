var thinky = require('thinky'),
    config = require('config');

thinky.init({
  db: config.database.db,
  host: config.database.host,
  port: config.database.port
});

exports.poolSize = config.database.pool || 10;

var Book = thinky.createModel('books',{
  id:        String,
  language:  String,
  name:      String,
  publisher: String,
  rights:    String,
  title:     String,
  authors:   [String],
  subjects:  [String],
  updated_at : {_type: Number , default: function() { return Date.now() } }
});

exports.addBook = function(book, done) {
  Book.filter({id: book.id}).run(function(error, books){

    if(error) {return done(error);}

    if(books.length == 0){
      var newBook = new Book(book);
      newBook.save(function(error, result){
        if(error){
          done(error);
        } else {
          done(null, result);
        }
      });
    } else {
      done({ error: true, message: book.title + " already exist"});
    }

  });
}


