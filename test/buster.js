var config = module.exports;

config['Gutenberg'] = {

  rootPath: "../",
  environment: "node",
  sources: [
    "lib/rdf-parser.js"
  ],
  tests: [
    "test/test-*.js"
  ]

}
