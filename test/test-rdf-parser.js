var buster = require('buster'),
    assert = buster.referee.assert,
    rdfparser = require('../lib/rdf-parser.js');


buster.testCase("RDF Parser", {

  "parse a RDF file": function(done) {
    var filename = "test/data/pg132.rdf";

    rdfparser(filename, done(function(err, book){
      assert.equals("132", book.id);
      assert.equals(['Sunzi (6th cent. BC)', 'Giles, Lionel'], book.authors);
    }));
  }

});
